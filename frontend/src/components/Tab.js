import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Tab extends Component {
    static propTypes = {
      activeTab: PropTypes.string.isRequired,
      button: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
    };
  
    onClick = () => {
      const { button, onClick } = this.props;
      onClick(button);
    }
  
    render() {
      const {
        onClick,
        props: {
          activeTab,
          button,
          background,
        },
      } = this;
  
      let className = 'tab-list-item';
  
      if (activeTab === button) {
        className += ' tab-list-active';
      }
  
      // if (background===1) {
      //   className+= ' test'
      // }

      return (
        <li 
          className={className}
          onClick={onClick}
        >
        {button}
        </li>
      );
    }
  }
  
export default Tab;