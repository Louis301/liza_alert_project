import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tab from '../components/Tab';

class Tabs extends Component {
    static propTypes = {
      children: PropTypes.instanceOf(Array).isRequired,
    }
  
    constructor(props) {
      super(props);
  
      this.state = {
        activeTab: this.props.children[0].props.button,
      };
    }
  
    onClickTabItem = (tab) => {
      this.setState({ activeTab: tab });
    }

    render() {
        const {
          onClickTabItem,
          props: {
            children,
          },
          state: {
            activeTab,
          }
        } = this;
    
        return (
          <div className="tabs">
            <ol className="tab-list">
              {children.map((child, index) => {
                const { button } = child.props;
   
                return (
                  <Tab
                    activeTab={activeTab}
                    key={button}
                    button={button}
                    onClick={onClickTabItem}

                  />
                );
              })}
              </ol>
            <div className="tab-content">
              {children.map((child) => {
                if (child.props.button !== activeTab) return undefined;
                return child.props.children;
              })}
            </div>
          </div>
        );
      }
    }
    
export default Tabs;