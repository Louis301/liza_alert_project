import React, { useRef, useEffect, useState } from 'react';
import "@maptiler/sdk/dist/maptiler-sdk.css";
import * as maptilersdk from '@maptiler/sdk';
import maplibregl from 'maplibre-gl';
import { GeocodingControl } from "@maptiler/geocoding-control/react";
import { createMapLibreGlMapController } from "@maptiler/geocoding-control/maplibregl-controller";
import "@maptiler/geocoding-control/style.css";
import 'maplibre-gl/dist/maplibre-gl.css';
import '../css/style.css';

export default function Map() {
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng] = useState(40.9714);
  const [lat] = useState(56.9972);
  const [zoom] = useState(12);
  const [API_KEY] = useState('iI4HsAfUaNpw0azb2qQs');
  const [mapController, setMapController] = useState();

  useEffect(() => {
    if (map.current) return; // stops map from intializing more than once

    maptilersdk.config.apiKey = 'iI4HsAfUaNpw0azb2qQs';
    map.current = new maptilersdk.Map({
      container: mapContainer.current,
      style: maptilersdk.MapStyle.OUTDOOR,
      center: [lng, lat],
      zoom: zoom,
    });

    setMapController(createMapLibreGlMapController(map.current, maplibregl));

  }, [API_KEY, lng, lat, zoom]);

  return (
    <div className="map-wrap">
      <div className="geocoding">
        <GeocodingControl apiKey={API_KEY} mapController={mapController} />
      </div>
      <div ref={mapContainer} className="map" />
    </div>
  );
}