import React from 'react';
import ReactDOMClient from 'react-dom/client';
import Tabs from "./components/Tabs";
import './css/index.css';
import Map from './components/Map';
import './css/style.css';

function App() {
    return (
      <div className='MapVision'>
        <div className='leftPanel' >
          <div className='Plug'></div>
          <div className='Plug2'></div>
          <Tabs>
            <div button="1" src="C:\Users\user\Desktop\liza_alert_project\frontend\src\img\loc.png">
            <div className='FirstBlock'>
                <h1>Введите место поисков</h1>
                <input className='Search' placeholder="Место" type="text"/>
                <h2>Для настройки зоны покрытия
                    необходимо перейти во 
                    вкладку “Настройки”</h2>
                  <button className="menu__btn">Подобрать место для лагеря</button>
            </div>
            </div>
            <div button="2">
            <div className="SecondBlock">
                <h1>Введите исходные данные</h1>
                <input placeholder='Тип антенны'/>
                <input placeholder='Мощность сигнала'/>
                <input placeholder='Азимут'/>
                <input placeholder='Высота подвеса'/>
                <input placeholder='Усиление антенны'/>
            </div>
            </div>
            <div button="3">
            <div className="ThirdBlock">3</div>
            </div>
          </Tabs>
        </div>
        <Map></Map>
      </div>
    );
  }
  
const app = ReactDOMClient.createRoot(document.getElementById("app"))

app.render(<App />)